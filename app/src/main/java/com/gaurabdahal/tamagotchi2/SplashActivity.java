//Gaurab Dahal

package com.gaurabdahal.tamagotchi2;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

public class SplashActivity extends AppCompatActivity {
    private Handler mHandler = new Handler();
    boolean quit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        quit = false;
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!quit) {
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    finish();
                }
            }
        }, 3000);
    }


    @Override
    public void onBackPressed(){
        quit = true;
        super.onBackPressed();
    }
}