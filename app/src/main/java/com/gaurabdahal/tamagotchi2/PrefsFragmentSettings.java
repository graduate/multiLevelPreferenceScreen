//Gaurab Dahal

package com.gaurabdahal.tamagotchi2;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;

/**
 * Created by gaurabdahal on 7/11/17.
 */
public class PrefsFragmentSettings  extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener{

    Context context;

    public PrefsFragmentSettings(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.prefs_fragment_settings);
    }

    public void onResume(){
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);

        Preference prefs;
        prefs = getPreferenceScreen().findPreference("key_restart_game");
        prefs.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                new AlertDialog.Builder(getActivity())
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("Restarting Application")
                        .setMessage("Are you sure you want to restart?")
                        .setPositiveButton("yes",new DialogInterface.OnClickListener(){
                            @Override
                            public void onClick(DialogInterface dialog, int which){
                                Intent intent = new Intent(getActivity(), SplashActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); //this will always start your activity as a new task
                                startActivity(intent);
                            }
                        })

                        .setNegativeButton("No",null).show();

        return true;
            }
        });


    }

    public void onPause(){
        super.onPause();

    }

    /**
     * Called when a shared preference is changed, added, or removed. This
     * may be called even if a preference is set to its existing value.
     */
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

        if(key.equals("key_speech_enabled")){
            Assets.tts_enabled=sharedPreferences.getBoolean("key_speech_enabled",true);
        }
    }
}
