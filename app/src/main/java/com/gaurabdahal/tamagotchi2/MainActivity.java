//Gaurab Dahal

package com.gaurabdahal.tamagotchi2;

import android.content.Intent;
import android.media.MediaPlayer;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    MainView v;

    public  static TextToSpeech tts;
    public static boolean tts_initialized;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);

        // Create the TTS object
        tts_initialized = false;
        tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    // Set the language to use
                    tts.setLanguage(Locale.UK);
                    tts_initialized = true;
                }
            }
        });


    }

    @Override
    protected void onPause () {
        if (! isFinishing()) {
            // Allow the gator to get hungry in 5 seconds
            float curTime = System.nanoTime() / 1000000000f;
            Assets.timeWhenHungry = curTime + Assets.totalHappinessCollected;

            // Use pre-Android 5.0 version?
            //if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.LOLLIPOP) {
            // do things the old way
            //}
            //else {
            // do things the Lolipop way
            //}

            // This is an implicit Intent - this worked in Android 4 and below, no longer works in Android 5
            //startService (new Intent(GatorService.ACTION_START));

            // This is an explicit Intent - use this to start a service in Android 5
            Intent intent = new Intent(this, TamagotchiService.class);
            intent.setAction(TamagotchiService.ACTION_START);
            startService(intent);
        }
        super.onPause();

        if( Assets.mp != null) {
            Assets.mp.pause();
            Assets.mp.release();
            Assets.mp = null;
        }
        if(v!=null)
            v.pause();
    }

    @Override
    protected void onResume () {
        super.onResume();

        if( Assets.mp != null){
            Assets.mp.start();
        }else{
            Assets.mp = MediaPlayer.create(this, R.raw.bgmusic);
            Assets.mp.setLooping(true);
            Assets.mp.start();
            Assets.mp.setVolume(0.3f, 0.3f);
        }
        if(v!=null)
            v.resume();
        else {
            v = new MainView(this);
            setContentView(v);
        }
    }
}
