//Gaurab Dahal

package com.gaurabdahal.tamagotchi2;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.SurfaceHolder;

/**
 * Created by gaurabdahal on 7/24/17.
 */
public class MainThread extends Thread {
    private SurfaceHolder holder;
    private Handler handler;
    private boolean isRunning = false;
    boolean data_initialized;
    private static final Object lock = new Object();
    Context context;
    Paint paint;
    int tx,ty;
    boolean touched;
    static boolean eggCracked=false;
    float gameTimerProgress;



    public MainThread (SurfaceHolder surfaceHolder, Context context) {
        holder = surfaceHolder;
        this.context = context;
        handler = new Handler();
        data_initialized=false;
        gameTimerProgress=System.nanoTime() / 1000000000f;
        tx=ty=0;
        touched=false;
    }

    public void setRunning(boolean b) {
        isRunning = b;	// no need to synchronize this since this is the only line of code to writes this variable
    }

    public void touch(int x, int y){
        tx=x;
        ty=y;
        touched=true;
    }

    public boolean optionTouched (Canvas canvas, int touchx, int touchy) {
        boolean touched = false;
        float dis = (float)(Math.sqrt (Math.pow((touchx - (canvas.getWidth()-50-Assets.option.getWidth()/2)),2) + Math.pow((touchy - (20+Assets.option.getHeight()/2)),2)));
        // Is this close enough for a kill?
        if (dis <= Assets.option.getWidth()*0.5f) {
            touched=true;
        }
        return (touched);
    }


    @Override
    public void run() {
        Looper.prepare();
        while (isRunning) {
            // Lock the canvas before drawing
            if(holder !=null) {
                Canvas canvas = holder.lockCanvas();

                if (canvas != null) {
                    // Perform drawing operations on the canvas
                    render(canvas);

                    holder.unlockCanvasAndPost(canvas);
                }
            }
        }
    }

    public void render(Canvas canvas){
        if (!data_initialized) {
            loadData(canvas);
            data_initialized = true;
        }


        canvas.drawBitmap(Assets.background, 0, 0, null);
        canvas.drawBitmap(Assets.option, canvas.getWidth()-Assets.option.getWidth()-50, 20, null);

        switch (Assets.state) {
            case Starting:
                Assets.tamagotchi.init(canvas);
                Assets.state = Assets.GameState.Running;
                break;
            case Running:
                if(eggCracked)
                    if(Assets.tamagotchi.state != Tamagotchi.State.Eating) {
                        canvas.drawBitmap(Assets.food, 50, 10, null);
                    }
                float currentTime = System.nanoTime() / 1000000000f;
                if (!eggCracked && (currentTime - gameTimerProgress >= Assets.hatchingTime)) {
                    if(Assets.is_sound_loaded)
                        Assets.soundPool.play(Assets.sound_crack, 1, 1, 1, 0, 1);
                    eggCracked = true;
                }

                if(touched){
                    if(foodTouched(canvas,tx,ty)){
                        Assets.tamagotchi.feed();
                    }
                    if(optionTouched(canvas,tx,ty)){
                        Intent mIntent = new Intent(context,PrefsActivity.class);
                        context.startActivity(mIntent);
                    }
                    touched=false;
                }
                break;
            default:
                Assets.state = Assets.GameState.Running;
                break;
        }


        Assets.tamagotchi.eggStage(canvas);
        Assets.tamagotchi.eggCrack(canvas);
        Assets.tamagotchi.happy(canvas);
        Assets.tamagotchi.hungry(canvas);
        Assets.tamagotchi.eating(canvas);
        Assets.tamagotchi.setSpeed(canvas);
        Assets.tamagotchi.move(canvas);
    }

    public boolean foodTouched (Canvas canvas, int touchx, int touchy) {
        boolean touched = false;
        float distance = (float)(Math.sqrt ((touchx - (50+Assets.food.getWidth()/2)) * (touchx - (50+Assets.food.getWidth()/2)) + (touchy - (10+Assets.food.getHeight()/2)) * (touchy - (10+Assets.food.getHeight()/2))));
        if (distance <= Assets.food.getWidth()*0.5f) {
            if(Assets.tamagotchi.state != Tamagotchi.State.Eating) {
                touched = true;
            }
        }
        return (touched);
    }

    // Loads graphics, etc. used in game
    private void loadData (Canvas canvas) {
        Bitmap bmp;
        int newWidth, newHeight;
        float scaleFactor;

        // Create a paint object for drawing vector graphics
        paint = new Paint();


        bmp = BitmapFactory.decodeResource(context.getResources(), R.drawable.background);
        Assets.background = Bitmap.createScaledBitmap (bmp, canvas.getWidth(), canvas.getHeight(), false);
        bmp = null;

        // Load egg
        bmp = BitmapFactory.decodeResource(context.getResources(), R.drawable.egg);
        newWidth = (int)(canvas.getWidth() * 0.2f);
        scaleFactor = (float)newWidth / bmp.getWidth();
        newHeight = (int)(bmp.getHeight() * scaleFactor);
        Assets.egg = Bitmap.createScaledBitmap (bmp, newWidth, newHeight, false);
        bmp = null;

        // Load Tamagotchi
        bmp = BitmapFactory.decodeResource (context.getResources(), R.drawable.food);
        newWidth = (int)(canvas.getWidth() * 0.15f);
        scaleFactor = (float)newWidth / bmp.getWidth();
        newHeight = (int)(bmp.getHeight() * scaleFactor);
        Assets.food = Bitmap.createScaledBitmap (bmp, newWidth, newHeight, false);
        // Delete the original
        bmp = null;

        // Load Tamagotchi
        bmp = BitmapFactory.decodeResource (context.getResources(), R.drawable.option);
        newWidth = (int)(canvas.getWidth() * 0.1f);
        scaleFactor = (float)newWidth / bmp.getWidth();
        newHeight = (int)(bmp.getHeight() * scaleFactor);
        Assets.option = Bitmap.createScaledBitmap (bmp, newWidth, newHeight, false);
        // Delete the original
        bmp = null;


        // Load Tamagotchi
        bmp = BitmapFactory.decodeResource (context.getResources(), R.drawable.t_eating1);
        newWidth = (int)(canvas.getWidth() * 0.2f);
        scaleFactor = (float)newWidth / bmp.getWidth();
        newHeight = (int)(bmp.getHeight() * scaleFactor);
        Assets.eating1 = Bitmap.createScaledBitmap (bmp, newWidth, newHeight, false);

        bmp = BitmapFactory.decodeResource (context.getResources(), R.drawable.t_eating2);
        Assets.eating2 = Bitmap.createScaledBitmap (bmp, newWidth, newHeight, false);

        bmp = BitmapFactory.decodeResource (context.getResources(), R.drawable.happy1);
        Assets.happy1 = Bitmap.createScaledBitmap (bmp, newWidth, newHeight, false);

        bmp = BitmapFactory.decodeResource (context.getResources(), R.drawable.happy2);
        Assets.happy2 = Bitmap.createScaledBitmap(bmp, newWidth, newHeight, false);

        bmp = BitmapFactory.decodeResource (context.getResources(), R.drawable.hungry1);
        Assets.hungry1 = Bitmap.createScaledBitmap (bmp, newWidth, newHeight, false);

        bmp = BitmapFactory.decodeResource (context.getResources(), R.drawable.hungry2);
        Assets.hungry2 = Bitmap.createScaledBitmap (bmp, newWidth, newHeight, false);
        bmp = null;

        // Create a tamagotchi
        Assets.tamagotchi = new Tamagotchi(context);
    }



}
