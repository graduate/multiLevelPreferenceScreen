//Gaurab Dahal
package com.gaurabdahal.tamagotchi2;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Looper;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by gaurabdahal on 7/24/17.
 */
public class Tamagotchi {
    Context context;
    String DEBUGKEY = "TAMAGOTCHI";

    enum State {
        Egg,
        EggCracking,
        Happy,
        Hungry,
        Eating,
    };

    static State state;
    Bitmap currentBm1,currentBm2;
    float secondTracker, secondTracker2,animateTimer;
    double speed;
    int x,y;
    boolean moveRight=true;


    public Tamagotchi (Context context) {
        state = State.Egg;
        this.context = context;
        currentBm1=Assets.egg;
        currentBm2=Assets.egg;
        secondTracker=secondTracker2=0;

    }

    public void init(Canvas canvas){
        y = (int) canvas.getHeight()/2;
        x = 0;
        animateTimer = System.nanoTime() / 1000000000f;
        currentBm1 = Assets.egg;
        currentBm2 = Assets.egg;
    }

    public void eggStage(Canvas canvas){
        if(state == State.Egg) {
            float currentTime = System.nanoTime();
            if (MainThread.eggCracked) {
                state = State.EggCracking;
            }
        }
    }

    public void eggCrack(Canvas canvas){
        if(state == State.EggCracking) {
            currentBm1 = Assets.happy1;
            currentBm2 = Assets.happy2;
            Assets.totalHappinessCollected=Assets.happyDuration; // initially hatched tamagotchi is happy.
            state = State.Happy;
            secondTracker2=System.nanoTime() / 1000000000f;
        }
    }

    public void happy(Canvas canvas){
        if(state==State.Happy){
            currentBm1 = Assets.happy1;
            currentBm2 = Assets.happy2;
            float curTime = System.nanoTime() / 1000000000f;
            if(curTime - secondTracker >= 1){
                Assets.totalHappinessCollected--;
                secondTracker=curTime;
            }
            if(Assets.totalHappinessCollected<1){
                state = State.Hungry;
                if(Assets.tts_enabled)
                    speak("I am Hungry");
            }

            //play hungry sound every 3 seconds
            curTime = System.nanoTime() / 1000000000f;
            if(curTime - secondTracker2 >= 3) {
                Assets.soundPool.play(Assets.sound_happy, 0.1f, 0.1f, 1, 0, 1);
                secondTracker2=curTime;
            }
        }
    }

    public void hungry(Canvas canvas){
        if(state==State.Hungry){
            currentBm1 = Assets.hungry1;
            currentBm2 = Assets.hungry2;

            //play hungry sound every 3 seconds
            float curTime = System.nanoTime() / 1000000000f;
            if(curTime - secondTracker >= 3) {
                Assets.soundPool.play(Assets.sound_sad, 1, 1, 1, 0, 1);
                secondTracker=curTime;
            }
        }
    }

    public void feed(){
        if(Assets.totalHappinessCollected<Assets.maxHappinessDuration) {
            Assets.gameTimerProgress = System.nanoTime() / 1000000000f;

            Assets.totalHappinessCollected += Assets.happyDuration;
            if(Assets.totalHappinessCollected>=Assets.maxHappinessDuration) {
                Assets.totalHappinessCollected = Assets.maxHappinessDuration;
                //Toast.makeText(context,"I am full. (happy for 120s)", Toast.LENGTH_SHORT).show();
            }
            state = State.Eating;
        }
    }

    public void eating(Canvas canvas){
        if(state == State.Eating){

            currentBm1 = Assets.eating1;
            currentBm2 = Assets.eating2;
            //show eating animation
            eatingAnimation(canvas);


            //play eating sound while eating
            float currentTime = System.nanoTime() / 1000000000f;
            if ((currentTime - Assets.gameTimerProgress <= Assets.eatingAnimationTime)) {
                if((int)( currentTime - Assets.gameTimerProgress) == Assets.eatingAnimationTime){
                    //play happy sound when done eating
                    //Assets.soundPool.play(Assets.sound_happy, 0.5f, 0.5f, 1, 0, 1);

                } else{
                    //play hungry sound every 3 seconds
                    if(currentTime - secondTracker >1) {
                        if(Assets.is_sound_loaded)
                            Assets.soundPool.play(Assets.sound_eating, 0.7f, 0.7f, 1, 0, 1);
                        secondTracker=currentTime;
                    }
                }
            }else{
                if(Assets.tts_enabled)
                    speak("I am Very Happy");
                state = State.Happy;
                Assets.gameTimerProgress = currentTime;

            }

            //play hungry sound every 3 seconds
            String text = "Eating for "+Assets.eatingAnimationTime+" s";
            displayProgress(canvas, text);
        }
    }

    public void eatingAnimation(Canvas canvas){
        float curTime = System.currentTimeMillis()/ 100 %10;
        Log.i(DEBUGKEY,curTime+"");
        if(curTime%2==0) {
            canvas.drawBitmap(Assets.eating1, x - Assets.eating1.getWidth(), y - Assets.eating1.getHeight() / 2, null);
        }else{
            canvas.drawBitmap(Assets.eating2, x - Assets.eating2.getWidth
                    (), y - Assets.eating2.getHeight() / 2, null);
        }
    }

    public void setSpeed(Canvas canvas){
        if(state == State.Happy || state == State.Egg) speed = canvas.getWidth() / 4; // no faster than 1/4 a screen per second
        else if(state == State.Hungry) speed = canvas.getWidth() / 8; // no faster than 1/8 a screen per second
    }

    public void move(Canvas canvas){
        y = (int) canvas.getHeight()/2;
        Log.i(DEBUGKEY, "State =" + state + ", totalHappiness=" + Assets.totalHappinessCollected);
        if(state == State.Eating){

        }else {
            float curTime = System.nanoTime() / 1000000000f;
            float elapsedTime = curTime - animateTimer;
            animateTimer = curTime;
            // Compute the amount of pixels to move (vertically down the screen)
            if (moveRight) {
                x += (speed * elapsedTime);
            } else {
                x -= (speed * elapsedTime);
            }
            if (x >= canvas.getWidth()) {
                x = canvas.getWidth();
                moveRight = false;
            }
            if (x <currentBm1.getWidth()) {
                x = currentBm1.getWidth();
                moveRight = true;
            }
            Log.i(DEBUGKEY,"x="+x);
            // Draw tamagotchi on screen
            curTime = System.currentTimeMillis()/ 100 %10;
            if(curTime%2==0) {
                canvas.drawBitmap(currentBm1, x - currentBm1.getWidth(), y - currentBm1.getHeight() / 2, null);
            }else{
                canvas.drawBitmap(currentBm2, x - currentBm2.getWidth(), y - currentBm2.getHeight() / 2, null);
            }
            String text = "Happiness timer : " + Assets.totalHappinessCollected + " s";
            displayProgress(canvas, text);
        }
    }
    public void displayProgress(Canvas canvas,String text){
        Paint paint = new Paint();
        //paint.setColor(Color.WHITE);
        //paint.setStyle(Style.FILL);
        //canvas.drawPaint(paint);

        paint.setColor(Color.WHITE);
        paint.setTextSize(20);
        canvas.drawText(text, 50, canvas.getHeight() - 40, paint);
    }

    public void speak(String toSpeak) {
        if (MainActivity.tts_initialized) {
            if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.LOLLIPOP)
                MainActivity.tts.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
            else
                MainActivity.tts.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null, "1");
        }
    }
}
