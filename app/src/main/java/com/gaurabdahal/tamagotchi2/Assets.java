//Gaurab Dahal

package com.gaurabdahal.tamagotchi2;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.media.SoundPool;

/**
 * Created by gaurabdahal on 7/24/17.
 */
public class Assets {

    public static Context context;

    // States of the Game Screen
    enum GameState {
        Starting,
        Running,
    };
    static GameState state;

    public static MediaPlayer mp=null;
    public static boolean tts_enabled=true;

    static Bitmap background;   //background image
    static Bitmap egg;          //egg state image
    static Bitmap happy1;
    static Bitmap happy2;       //happy image
    static Bitmap hungry1;
    static Bitmap hungry2;      //sad image
    static Bitmap eating1;      //eating image 1 for animation
    static Bitmap eating2;      //eating image 2 for animation
    static Bitmap food;
    static Bitmap option;
    static Tamagotchi tamagotchi;

    static int totalHappinessCollected=0;
    static int maxHappinessDuration=120;
    static int eatingAnimationTime = 5;
    public static float timeWhenHungry;

    static int hatchingTime = 7; //hatch after 7 seconds of being egg;
    static int happyDuration = 15; //happy for 10 seconds;

    static boolean is_sound_loaded=false;

    static SoundPool soundPool;
    static int sound_happy;
    static int sound_sad;
    static int sound_eating;
    static int sound_crack;

    static float gameTimerProgress;
    // True when the service is processing a wait timer
    public static volatile boolean serviceIsProcessing;
}
