//Gaurab Dahal

package com.gaurabdahal.tamagotchi2;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by gaurabdahal on 7/24/17.
 */
public class MainView extends SurfaceView implements SurfaceHolder.Callback  {

    Context context;  //Reference to MainActivity
    private SurfaceHolder holder = null;
    private MainThread t = null;
    int num_sounds_loaded = 0;


    public MainView(Context context) {
        super(context);
        this.context = context;

        holder = getHolder();
        holder.addCallback(this);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            Assets.soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
        }
        else {
            AudioAttributes attributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_GAME)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();
            Assets.soundPool = new SoundPool.Builder()
                    .setAudioAttributes(attributes)
                    .build();

        }

        Assets.sound_happy = Assets.soundPool.load(context, R.raw.winner, 1);
        Assets.sound_sad = Assets.soundPool.load(context, R.raw.growling, 1);
        Assets.sound_eating = Assets.soundPool.load(context, R.raw.eating, 1);
        Assets.sound_crack = Assets.soundPool.load(context, R.raw.crack, 1);

        Assets.soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                num_sounds_loaded++;
                if (num_sounds_loaded == 4) {
                    Assets.is_sound_loaded = true;
                }
            }
        });
        Assets.state = Assets.GameState.Starting;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (t == null) {
            t = new MainThread(holder, context);
            t.setRunning(true);
            t.start();
            setFocusable(true); // make sure we get events
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        float x, y;
        int action = event.getAction();
        x = event.getX();
        y = event.getY();
        if (action == MotionEvent.ACTION_UP) {
            if (t != null)
                t.touch ((int)x, (int)y);
        }
        return true; // to indicate we have handled this event
    }



    public void pause ()
    {
        if(t!=null) {
            t.setRunning(false);
            while (true) {
                try {
                    t.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                break;
            }

        }
        t = null;
    }

    public void resume(){
        if (t == null) {
            t = new MainThread(holder, context);
            t.setRunning(true);
            t.start();
            setFocusable(true); // make sure we get events
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }
}
